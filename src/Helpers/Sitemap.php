<?php

namespace Workhouse\LaravelSitemap\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;


class Sitemap
{

	const IGNORED_BINDING_CLASSES = [
		'Illuminate\Http\Request'
	];


	/*
		Get the factories (if any) configured
	*/
	public static function getFactories()
	{
		return config('sitemap.factories', []);
	}


	/*

	*/
	public static function getRouteUrls()
	{
		$sitemapRoutes = [];
		$routes = Route::getRoutes();

		foreach($routes as $route)
		{
			$middleware = $route->middleware() ?? [];
			
			if(in_array('GET', $route->methods()) && in_array('sitemap', $middleware) && !in_array('auth', $middleware))
			{
				// Does this route have any paramteres?
				$parameterNames = $route->parameterNames();
				$parameters = $route->signatureParameters();

				if(!$parameterNames)
				{
					$sitemapRoutes[] = [
						'url' => url()->toRoute($route, [], true)
					];
				}
				else
				{
					$validBindings = [];

					// Filter out any irrelevent classes
					foreach($parameters as $parameter)
					{
						if($parameter->hasType())
						{
							$class = $parameter->getType()->getName();

							if(!in_array($class, self::IGNORED_BINDING_CLASSES))
							{
								if(is_subclass_of($class, Model::class))
								{
									$validBindings[] = $class;
								}
							}
						}
					}

					// If we just have the one paramter, let's use the default scope to populate the sitemap
					if(sizeof($validBindings) == 1)
					{
						$collection = $validBindings[0]::get();
						
						if($collection->isNotEmpty())
						{
							foreach($collection as $item)
							{
								$sitemapRoutes[] = [
									'url' => url()->toRoute($route, $item, true),
									'last_mod' => $item->updated_at ?? null
								];
							}
						}
					}
				}
			}
		}

		return $sitemapRoutes;
	}


	/*

	*/
	public static function getFactoryUrls($forFactory)
	{
		$factories = self::getFactories();
		$factory = $factories[$forFactory] ?? null;

		if(!$factory)
		{
			return [];
		}
		
		$sitemapRoutes = [];

		$factory = new $factory;

		$query = $factory->query();
		$collection = $query->get();

		foreach($collection as $item)
		{
			$url = $factory->route($item);

			if(!empty($url))
			{
				$sitemapRoutes[] = [
					'url' => $url,
					'last_mod' => $factory->lastMod($item)
				];
			}
		}

		return $sitemapRoutes;
	}


	/*

	*/
	public static function getSitemap()
	{
		
	}
}