<?php

use Workhouse\LaravelSitemap\Http\Controllers\SitemapController;

Route::get('sitemap.xml', [ SitemapController::class, 'index' ]);
Route::get('sitemap_{factoryKey}.xml', [ SitemapController::class, 'show' ])->name('wh-sitemap.show');
