<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   @foreach($sitemapRoutes as $route)
      <url>
         <loc>{!! htmlspecialchars($route['url'], ENT_XML1, 'UTF-8') !!}</loc>
         @if(isset($route['last_mod']) && $route['last_mod'])
            <lastmod>{{ $route['last_mod']->toDateString() }}</lastmod>
         @endif
      </url>
   @endforeach
</urlset>