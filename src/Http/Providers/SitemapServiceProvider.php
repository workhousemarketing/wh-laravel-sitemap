<?php

namespace Workhouse\LaravelSitemap\Http\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use Workhouse\LaravelSitemap\Http\Middleware\Sitemap as SitemapMiddleware;
use Workhouse\LaravelSitemap\Console\Commands\SitemapStatus;

class SitemapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load required package files
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'wh-sitemap');
    	$this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');

        // Alias our middleware so it can be applied to routes
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('sitemap', SitemapMiddleware::class);

        // Sitemap default config file
        $this->publishes([
            $this->getConfigPath() => config_path('sitemap.php'),
        ], 'config');


        if($this->app->runningInConsole())
        {
            $this->commands([
                SitemapStatus::class,
            ]);
        }
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom($this->getConfigPath(), 'sitemap');
    }

    
    /*
        Returns the path for the package config file
    */
    private function getConfigPath()
    {
        return __DIR__ . '/../../config/config.php';
    }
}
