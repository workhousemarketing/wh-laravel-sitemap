<?php

namespace Workhouse\LaravelSitemap\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Workhouse\LaravelSitemap\Helpers\Sitemap;


class SitemapController extends BaseController
{
	/*
		Default route for the sitemap, and will either show an index of multiple sitemaps
		or default to route sitemaps (depending on the configuration of the Laravel project)
	*/
	public function index(Request $request)
	{
		$factories = Sitemap::getFactories();

		if($factories)
		{
			// If we have factories, then just output an index now
			return view('wh-sitemap::index', compact('factories'))
				->withHeaders([ 'Content-Type' => 'application/xml' ]);
		}

		// Otherwise we can just output the Route URLs
		$sitemapRoutes = Sitemap::getRouteUrls();

		return view('wh-sitemap::show', compact('sitemapRoutes'))
			->withHeaders([ 'Content-Type' => 'application/xml' ]);
	}


	/*
		Show a specific sitemap, either for routes or based on Sitemap Factories
	*/
	public function show(Request $request, string $factoryKey)
	{
		if($factoryKey == 'default')
		{
			$sitemapRoutes = Sitemap::getRouteUrls();
		}
		else
		{
			$sitemapRoutes = Sitemap::getFactoryUrls($factoryKey);
		}

		if(empty($sitemapRoutes))
		{
			abort(404);
		}

		return view('wh-sitemap::show', compact('sitemapRoutes'))
			->withHeaders([ 'Content-Type' => 'application/xml' ]);
	}
}
