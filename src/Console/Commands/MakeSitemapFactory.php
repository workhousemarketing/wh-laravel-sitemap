<?php

namespace Workhouse\LaravelSitemap\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;


class MakeSitemapFactory extends GeneratorCommand
{
	protected $name = 'make:sitemap-factory';

	protected $description = 'Create a new Sitemap Factory class';

	protected $type = 'class';


	protected function getStub()
	{
		return __DIR__ . '/Stubs/make-sitemap.stub';
	}

	
	protected function getDefaultNamespace($rootNamespace)
	{
		return $rootNamespace . '\Sitemap';
	}
}