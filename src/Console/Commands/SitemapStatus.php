<?php

namespace Workhouse\LaravelSitemap\Console\Commands;

use Illuminate\Console\Command;
use Workhouse\LaravelSitemap\Helpers\Sitemap;

class SitemapStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show general information about the Sitemap being generated';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('URLs in your sitemap...');

        $count = count(Sitemap::getRouteUrls());
        $totalUrls = $count;
        $this->line(PHP_EOL . 'Default (route-based) URLs... ' . number_format($count));

        $factories = Sitemap::getFactories();

        if($factories)
        {
            foreach($factories as $key => $factory)
            {
                $count = count(Sitemap::getFactoryUrls($key));
                $totalUrls += $count;
                $this->line(ucfirst($key) . ' (factory) URLs... ' . number_format($count));
            }
        }

        $this->line('<fg=yellow>Total URLs... ' . number_format($totalUrls) . '</>');
    }
}
