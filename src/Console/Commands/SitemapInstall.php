<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SitemapInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the Sitemap package config file and other default resources';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Publishing the Sitemap config files...');
        
        $this->call('vendor:publish', [
            '--provider' => "Workhouse\LaravelSitemap\SitemapServiceProvider",
            '--tag' => "config"
        ]);
    }
}
