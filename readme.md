# Workhouse Laravel Sitemap

## How to include Routes in the sitemap

There are two main approaches to including routes in your sitemap.

### Route Middleware

Firstly, you can add the middleware `sitemap` to any declared routes in your project. WH Laravel Sitemap will automatically append these URLs to your sitemap

There's a few things to note when using this approach:

- Routes without any paramters will be added automatically
- If you're routes have a single parameter with a valid binding, WH Laravel Sitemap will automatically query that model (utilising the global scopes for that model) and add URLs for each model
 - Routes with multiple parameters will be skipped as the package won't be able to determine the correct relationship between the models (see Sitemap Factories for this)
- Routes with an Auth middleware also attached will be skipped (as these routes can't be seen publicly)

### Sitemap Factories

For routes with more complex parameter binding, or where you just need more control over what gets added to the sitemap, you can create custom Sitemap Factories. These classes will allow you to run custom model queries, and then allow you to provide the appropriate route for each model that is processed.

For example, if you have a route with both a category and a model id in the URL, you can use a Sitemap Factory to ensure the correct data is bound to the route for each model.

To create a Sitemap Factory, ...todo...

## How to access the sitemap

The index of your sitemap will be at /sitemap.xml

This index will either be your route sitemaps, or if you also have some Sitemap Factories, will be a directory which points to smaller sitemap files, for example:

- sitemap.xml
- sitemap_default.xml
- sitemap_myfactory.xml
- sitemap_my_other_factory.xml

## Configuration

You can publish a config file for the Sitemap plugin by running the artisan command `sitemap:install`

..todo.. descriptions on config options

## Artisan Commands

### sitemap:status

This command will genereate a summary of the sitemap that'll be generated, giving the number of URLs found for each subset of your sitemap (routes, factories, etc).

## Limitations of this package

- When using the sitemap middleware, this package does not limit the number URLs in a single Sitemap file and may produce an invalid sitemap
- There is no caching layer implemented right now, so sitemaps are generated in real time on each request
